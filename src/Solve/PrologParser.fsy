﻿%{
open Solve

open Solve.TermTypes
open Solve.TermTypes.Transformers

open Solve.Rule
open Solve.Rule.Transformers

type ParseResult =
    | RuleParseResult of Rule
    | CallParseResult of Goal
%}

%start start

%token <int> INT
%token <string> ATOM
%token <string> VAR
%token <string> STRING
%token IS
%token TRUE
%token FALSE
%token NULL
%token LEFT_BRACKET
%token RIGHT_BRACKET
%token LEFT_BRACK
%token RIGHT_BRACK
%token COLON
%token PLUS
%token MINUS
%token ASTERISK
%token SLASH
%token COMMA
%token DOT
%token QUESTION_MARK
%token EQUALS_SIGN
%token GREATER_THAN_SIGN
%token LESS_THAN_SIGN
%token EOF
%type <ParseResult option> start


%%

start: prog { $1 }

prog:
	| EOF { None }
	| QUESTION_MARK MINUS goal DOT { Some <| CallParseResult $3 }
	| fact { Some <| RuleParseResult $1 }
	| rule { Some <| RuleParseResult $1 }

fact:
	| signature { Rule($1, True) }
	
rule:
    | signature COLON MINUS body DOT { Rule($1, $4) }
	
signature:
	| ATOM LEFT_BRACKET parameterList RIGHT_BRACKET { Signature($1, $3) }

goal:
	| ATOM LEFT_BRACKET termList RIGHT_BRACKET { Solve.Rule.Goal(Solve.TermTypes.Structure($1, $3)) }

parameter:
    | term { Parameter($1) }

parameterList:
	| parameter { [$1] }
    | parameterList COMMA parameter { $1@[$3] }
	| { [] }

termList:
	| term { [$1] }
    | termList COMMA term { $1@[$3] }
	| { [] }

body:
    | TRUE { True }
	| FALSE { False }
	| goal { CallExpression($1) }
	| term IS calcExpr { CalcExpr($1, $3) }
	| body COMMA body { AndExpression($1, $3) }
	| body COLON body { OrExpression($1, $3) }
	| term EQUALS_SIGN term { EqExpr($1, $3) }
	| term GREATER_THAN_SIGN term { GrExpr($1, $3) }
	| term LESS_THAN_SIGN term { LeExpr($1, $3) }

calcExpr:
	| term { Value(CalcAny($1)) }
	| calcExpr PLUS calcExpr { Plus(CalcInner($1), CalcInner($3)) }
	| calcExpr MINUS calcExpr { Subsctruct(CalcInner($1), CalcInner($3)) }
	| calcExpr ASTERISK calcExpr { Multiply(CalcInner($1), CalcInner($3)) }
	| calcExpr SLASH calcExpr { Division(CalcInner($1), CalcInner($3)) }

term:
	| INT { TypedTerm(TypedNumberTerm(NumberTerm (float $1))) }
	| VAR { VariableTerm(Variable($1)) }
	| ATOM { TypedTerm(TypedAtomTerm(AtomTerm ($1))) }